#include <stdio.h>

int main(int argc, char *argv[])
{
    int i;
    int j;
    int k;

    int x;
    int y;
    char *str;
    char tab_observation[16];
    char tab_result[4][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
    char tab_test[] = {0,0,0,0};

    str = *(argv + 1);
    // ex : str = "4 3 2 1 1 2 2 2 4 3 2 1 1 2 2 2";

    i = 0;
    j = 0;
    while(str[i] != '\0' )
    {
        if (str[i] != ' ')
        {
            tab_observation[j] = str[i];
            j++;
        }
        i++;
    } 
    tab_observation[j] = '\0';


    // Vérifier que tab_observation est bien crée
    k = 0;
    while(tab_observation[k] != '\0')
    {
        printf("%c,",*(tab_observation + k));

        k++;
    }

    printf("\n");
    printf("\n");

    // Premiere moulinette pour determiner ce qui est sur
    i = 0;
    j = 0;
    //while(j < 16)
    while(tab_observation[i] != '\0')
    {
        if(j == 4)
        {
            j = 0;
        }

        if (i < 4){
            if (tab_observation[i] == '4')
            {
                tab_result[0][j] = 1;
                tab_result[1][j] = 2;
                tab_result[2][j] = 3;
                tab_result[3][j] = 4;
            }
            else if (tab_observation[i] == '1')
            {
                tab_result[0][j] = 4;
            }
        } 
        else if (i < 8){
            if (tab_observation[i] == '4')
            {
                tab_result[3][j] = 1;
                tab_result[2][j] = 2;
                tab_result[1][j] = 3;
                tab_result[0][j] = 4;
            }
            else if (tab_observation[i] == '1')
            {
                tab_result[3][j] = 4;
            }
        }
        else if (i < 12){
            if (tab_observation[i] == '4')
            {
                tab_result[j][0] = 1;
                tab_result[j][1] = 2;
                tab_result[j][2] = 3;
                tab_result[j][3] = 4;
            }
            else if (tab_observation[i] == '1')
            {
                tab_result[j][0] = 4;
            }
        }
        else {

            if (tab_observation[i] == '4')
            {
                tab_result[j][3] = 1;
                tab_result[j][2] = 2;
                tab_result[j][1] = 3;
                tab_result[j][0] = 4;
            }
            else if (tab_observation[i] == '1')
            {
                tab_result[j][3] = 4;
            }
        }

        i++;
        j++;
    }


    // Deuxieme moulinette pour determiner dans un deuxieme temps
    i = 0;
    j = 0;
    //while(j < 16)
    while(tab_observation[i] != '\0')
    {
        if(j == 4)
        {
            j = 0;
        }

        // Si observation est egal a 2
        if (tab_observation[i] == '2')
        {
            
            if (i < 4)
            {
                // Si le dernier est 4, le premier est 3
                if (tab_result[3][j] == 4)
                {
                    tab_result[0][j] = 3;
                }

                //Si le premier est 1, le deuxieme est 4
                if (tab_result[0][j] == 1)
                {
                    tab_result[1][j] = 4;
                }
            } 
            else if (i < 8)
            {
                // Si le dernier est 4, le premier est 3
                if (tab_result[0][j] == 4)
                {
                    tab_result[3][j] = 3;
                }

                //Si le premier est 1, le deuxieme est 4
                if (tab_result[3][j] == 1)
                {
                    tab_result[2][j] = 4;
                }
            }
            else if (i < 12)
            {
                // Si le dernier est 4, le premier est 3
                if (tab_result[j][3] == 4)
                {
                    tab_result[j][0] = 3;
                }

                //Si le premier est 1, le deuxieme est 4
                if (tab_result[j][0] == 1)
                {
                    tab_result[j][1] = 4;
                }
            }
            else {
                // Si le dernier est 4, le premier est 3
                if (tab_result[j][0] == 4)
                {
                    tab_result[j][3] = 3;
                }

                //Si le premier est 1, le deuxieme est 4
                if (tab_result[j][3] == 1)
                {
                    tab_result[j][2] = 4;
                }
            }
        }
        j++;
        i++;
    }

    // Troisieme moulinette pour deduire des resultats à partir des valeurs sures
    // Si la case est null, verifier les chiffres sur la ligne horizontal puis vertical

    x = 0;
    while(x < 4)
    {
        y = 0;
        while(y < 4)
        {
            tab_test[0] = 0;
            tab_test[1] = 0;
            tab_test[2] = 0;
            tab_test[3] = 0;
            if (tab_result[x][y] == 0){

                if(tab_result[0][y] != 0){
                    tab_test[tab_result[0][y] - 1] = tab_result[0][y];
                }
                if(tab_result[1][y] != 0){
                    tab_test[tab_result[1][y] - 1] = tab_result[1][y];
                }
                if(tab_result[2][y] != 0){
                    tab_test[tab_result[2][y] - 1] = tab_result[2][y];
                }
                if(tab_result[3][y] != 0){
                    tab_test[tab_result[3][y] - 1] = tab_result[3][y];
                }
                if(tab_result[x][0] != 0){
                    tab_test[tab_result[x][0] - 1] = tab_result[x][0];
                }
                if(tab_result[x][1] != 0){
                    tab_test[tab_result[x][1] - 1] = tab_result[x][1];
                }
                if(tab_result[x][2] != 0){
                    tab_test[tab_result[x][2] - 1] = tab_result[x][2];
                }
                if(tab_result[x][3] != 0){
                    tab_test[tab_result[x][3] - 1] = tab_result[x][3];
                }

                // afficher tmp
                /*
                i = 0;
                while(i < 4)
                {
                    printf("%d+", tab_test[i]);
                    i++;
                }
                printf("\n");
                */

                // Si toutes les cases sont remplies sauf une, c'est la bonne
                if (tab_test[0] == 0 && tab_test[1] != 0 && tab_test[2] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 1;
                }
                if (tab_test[1] == 0 && tab_test[0] != 0 && tab_test[2] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 2;
                }
                if (tab_test[2] == 0 && tab_test[0] != 0 && tab_test[1] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 3;
                }
                if (tab_test[3] == 0 && tab_test[0] != 0 && tab_test[1] != 0 && tab_test[2] != 0)
                {
                    tab_result[x][y] = 4;
                }

                /*
                printf("\n");
                printf("%d-", tab_result[0][y]);
                printf("%d-", tab_result[1][y]);
                printf("%d-", tab_result[2][y]);
                printf("%d-", tab_result[3][y]);
                printf("%d-", tab_result[x][0]);
                printf("%d-", tab_result[x][1]);
                printf("%d-", tab_result[x][2]);
                printf("%d-", tab_result[x][3]);
                printf("\n");
                */
            }
            //printf("%d ", tab_result[x][y]);

            y++;
        }
        x++;
    }

    // Copie exacte de la 3eme moulinette
    // Quatrieme moulinette pour deduire des resultats à partir des valeurs sures
    // Si la case est null, verifier les chiffres sur la ligne horizontal puis vertical

    x = 0;
    while(x < 4)
    {
        y = 0;
        while(y < 4)
        {
            tab_test[0] = 0;
            tab_test[1] = 0;
            tab_test[2] = 0;
            tab_test[3] = 0;
            if (tab_result[x][y] == 0){

                if(tab_result[0][y] != 0){
                    tab_test[tab_result[0][y] - 1] = tab_result[0][y];
                }
                if(tab_result[1][y] != 0){
                    tab_test[tab_result[1][y] - 1] = tab_result[1][y];
                }
                if(tab_result[2][y] != 0){
                    tab_test[tab_result[2][y] - 1] = tab_result[2][y];
                }
                if(tab_result[3][y] != 0){
                    tab_test[tab_result[3][y] - 1] = tab_result[3][y];
                }
                if(tab_result[x][0] != 0){
                    tab_test[tab_result[x][0] - 1] = tab_result[x][0];
                }
                if(tab_result[x][1] != 0){
                    tab_test[tab_result[x][1] - 1] = tab_result[x][1];
                }
                if(tab_result[x][2] != 0){
                    tab_test[tab_result[x][2] - 1] = tab_result[x][2];
                }
                if(tab_result[x][3] != 0){
                    tab_test[tab_result[x][3] - 1] = tab_result[x][3];
                }

                // Si toutes les cases sont remplies sauf une, c'est la bonne
                if (tab_test[0] == 0 && tab_test[1] != 0 && tab_test[2] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 1;
                }
                if (tab_test[1] == 0 && tab_test[0] != 0 && tab_test[2] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 2;
                }
                if (tab_test[2] == 0 && tab_test[0] != 0 && tab_test[1] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 3;
                }
                if (tab_test[3] == 0 && tab_test[0] != 0 && tab_test[1] != 0 && tab_test[2] != 0)
                {
                    tab_result[x][y] = 4;
                }

            }

            y++;
        }
        x++;
    }

    
    // Copie exacte de la 3eme moulinette
    // Cinquieme moulinette pour deduire des resultats à partir des valeurs sures
    // Si la case est null, verifier les chiffres sur la ligne horizontal puis vertical

    x = 0;
    while(x < 4)
    {
        y = 0;
        while(y < 4)
        {
            tab_test[0] = 0;
            tab_test[1] = 0;
            tab_test[2] = 0;
            tab_test[3] = 0;
            if (tab_result[x][y] == 0){

                if(tab_result[0][y] != 0){
                    tab_test[tab_result[0][y] - 1] = tab_result[0][y];
                }
                if(tab_result[1][y] != 0){
                    tab_test[tab_result[1][y] - 1] = tab_result[1][y];
                }
                if(tab_result[2][y] != 0){
                    tab_test[tab_result[2][y] - 1] = tab_result[2][y];
                }
                if(tab_result[3][y] != 0){
                    tab_test[tab_result[3][y] - 1] = tab_result[3][y];
                }
                if(tab_result[x][0] != 0){
                    tab_test[tab_result[x][0] - 1] = tab_result[x][0];
                }
                if(tab_result[x][1] != 0){
                    tab_test[tab_result[x][1] - 1] = tab_result[x][1];
                }
                if(tab_result[x][2] != 0){
                    tab_test[tab_result[x][2] - 1] = tab_result[x][2];
                }
                if(tab_result[x][3] != 0){
                    tab_test[tab_result[x][3] - 1] = tab_result[x][3];
                }

                // Si toutes les cases sont remplies sauf une, c'est la bonne
                if (tab_test[0] == 0 && tab_test[1] != 0 && tab_test[2] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 1;
                }
                if (tab_test[1] == 0 && tab_test[0] != 0 && tab_test[2] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 2;
                }
                if (tab_test[2] == 0 && tab_test[0] != 0 && tab_test[1] != 0 && tab_test[3] != 0)
                {
                    tab_result[x][y] = 3;
                }
                if (tab_test[3] == 0 && tab_test[0] != 0 && tab_test[1] != 0 && tab_test[2] != 0)
                {
                    tab_result[x][y] = 4;
                }

            }

            y++;
        }
        x++;
    }

    // Afficher le tableau de resultat final
    x = 0;
    while(x < 4)
    {
        y = 0;
        while(y < 4)
        {
            printf("%d ", tab_result[x][y]);

            y++;
        }
        printf("\n");
        x++;
    }

    return (0);
}
